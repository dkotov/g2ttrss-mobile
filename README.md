A [g2ttrss-mobile](https://github.com/g2ttrss/g2ttrss-mobile) fork
==============

This fork optimizes original app for non-smartphone browsers and reduces traffic about 12x (even by reducing functionality).